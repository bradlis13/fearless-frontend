function createCard(title, description, eventlocation, pictureUrl, startDate, endDate,) {
  const formattedStartDate = formatDate(startDate)
  const formattedEndDate = formatDate(endDate)

  return `
    <div class="card mb-4 shadow-lg">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <h6 class="card-subtitle mb-2 text-muted fs-6">${eventlocation}</h6>
        <p class="card-text">${description}</p>
        <p class="card-text"><small class="text-muted">${formattedStartDate} - ${formattedEndDate}</small></p>
      </div>
    </div>
  `;
}

function formatDate(dateString) {
  const date = new Date(dateString);
  const year = date.getFullYear();

  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');

  return `${month}/${day}/${year}`;
}

// Placeholder card
// Placeholder card
function createPlaceholderCard(id) {
  return `
    <div id="${id}" class="card mb-4 shadow-lg">
      <div class="skeleton card-img-top"></div>
      <div class="card-body">
        <h5 class="card-title skeleton"></h5>
        <h6 class="card-subtitle mb-2 text-muted fs-6 skeleton"></h6>
        <p class="card-text skeleton"></p>
        <p class="card-text"><small class="text-muted skeleton"></small></p>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';
  const columns = document.querySelectorAll('.col');
  let cardId = 0;

  // Fetch and display conference data
  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error('An Error Occured.');
    } else {
      const data = await response.json();
      let columnIndex = 0;

      for (let i = 0; i < data.conferences.length; i++) {
        columns[columnIndex].innerHTML += createPlaceholderCard('card' + cardId);
        columnIndex = (columnIndex + 1) % columns.length;
        cardId++;
    }

      cardId = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const eventlocation = details.conference.location.name;
          const html = createCard(title, description, eventlocation, pictureUrl, startDate, endDate);

          // Replace placeholder card with real data
          document.getElementById('card' + cardId).outerHTML = html;

          cardId++;
        }
      }
    }
  } catch (e) {
    console.error(e);
  }
});

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
          const optionElement = document.createElement('option');
          optionElement.value = location.id;
          optionElement.innerHTML = location.name;
          selectTag.appendChild(optionElement);
        }
      }
    } catch (error) {
      console.error(error);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const content = {};
      for (let [key, value] of formData.entries()) {
        if (key === 'location') {
          const selectedOption = document.querySelector('#location option:checked');
          content[key] = Number(selectedOption.value);
        } else {
          content[key] = value;
        }
      }
      const json = JSON.stringify(content);
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: 'POST',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      try {
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
          console.log(newConference);
        } else {
          const errorResponse = await response.json();
          console.error('Failed to create conference:', errorResponse);
        }
      } catch (error) {
        console.error(error);
      }
    });
  });
